package by.vsu.math.controller;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
@AllArgsConstructor
public class HomeController {

    @GetMapping("/")
    public @NotNull String homePage() {

        return "home";
    }

    @GetMapping("/login")
    public @NotNull String loginPage() {

        return "login";
    }
}