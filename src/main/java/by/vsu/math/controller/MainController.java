package by.vsu.math.controller;

import by.vsu.math.service.InfService;
import by.vsu.math.service.LimService;
import by.vsu.math.service.UserService;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
@AllArgsConstructor
public class MainController {

    private final UserService userService;
    private final InfService infService;
    private final LimService limService;

    @GetMapping("/main/table/users")
    public @NotNull String main(@NotNull Model model) {

        model.addAttribute("users", userService.findAll());

        return "main";
    }

    @GetMapping("/main/table/lim")
    public @NotNull String lim(@NotNull Model model) {

        model.addAttribute("lim", limService.findAll());

        return "tablelim";
    }

    @GetMapping("/main/table/inf")
    public @NotNull String inf(@NotNull Model model) {

        model.addAttribute("inf", infService.findAll());

        return "tableinf";
    }
}