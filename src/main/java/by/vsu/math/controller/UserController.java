package by.vsu.math.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import by.vsu.math.domain.User;
import by.vsu.math.service.UserService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class UserController {

	private final UserService userService;

	@PostMapping("/users")
	public @NotNull User save(@NotNull @ModelAttribute User user) {

		return userService.save(user);
	}

}
