package by.vsu.math.repo;

import by.vsu.math.domain.LimitedScores;
import org.springframework.data.repository.CrudRepository;

public interface LimScoresRepo extends CrudRepository<LimitedScores, Integer> {

}
