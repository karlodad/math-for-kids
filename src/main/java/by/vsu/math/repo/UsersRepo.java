package by.vsu.math.repo;

import by.vsu.math.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UsersRepo extends CrudRepository<User, Integer> {

    @Query(value = "SELECT nextval('unknowname')", nativeQuery = true)
    Long getNextUnknownNameKey();
}
