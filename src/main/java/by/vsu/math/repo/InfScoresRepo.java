package by.vsu.math.repo;

import by.vsu.math.domain.InfinityScores;
import org.springframework.data.repository.CrudRepository;

public interface InfScoresRepo extends CrudRepository<InfinityScores, Integer> {
}
