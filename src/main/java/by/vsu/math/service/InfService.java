package by.vsu.math.service;

import by.vsu.math.domain.InfinityScores;
import by.vsu.math.repo.InfScoresRepo;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class InfService {

    private final InfScoresRepo infScoresRepository;

    public @NotNull Iterable<InfinityScores> findAll() {

        return infScoresRepository.findAll();
    }
}
