package by.vsu.math.service;

import by.vsu.math.domain.LimitedScores;
import by.vsu.math.repo.LimScoresRepo;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class LimService {

    private final LimScoresRepo limScoresRepository;

    public @NotNull Iterable<LimitedScores> findAll() {

        return limScoresRepository.findAll();
    }
}
