package by.vsu.math.service;

import by.vsu.math.domain.User;
import by.vsu.math.repo.UsersRepo;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {

	private static final String UNKNOWN_PREFIX = "Young_padavan_";

	private final UsersRepo usersRepository;

	public @NotNull Iterable<User> findAll() {

		return usersRepository.findAll();
	}

	public @NotNull User save(@NotNull User user) {

		if (user.getName() == null || user.getName().isEmpty()) {
			user.setName(UNKNOWN_PREFIX + usersRepository.getNextUnknownNameKey());
		}

		return usersRepository.save(user);
	}
}
