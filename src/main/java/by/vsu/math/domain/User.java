package by.vsu.math.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @SequenceGenerator(name = "key1", sequenceName = "keyid", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "key1")
    private Integer id;

    @NotNull
    @Column(name = "name")
    private String name;
}
