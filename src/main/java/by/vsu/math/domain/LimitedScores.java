package by.vsu.math.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "limScores")
public class LimitedScores {

    @Id
    @SequenceGenerator(name = "key2", sequenceName = "keyid", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "key2")
    private Integer id;

    @NotNull
    @Column(name = "score")
    private Integer score;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;
}
